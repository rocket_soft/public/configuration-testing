FROM openjdk:11.0.11-oraclelinux8
ARG JAR_FILE=target/testing-*.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
