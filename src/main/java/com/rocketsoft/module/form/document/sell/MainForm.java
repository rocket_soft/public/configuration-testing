package com.rocketsoft.module.form.document.sell;

import com.rocketsoft.core.app.service.DbService;
import com.rocketsoft.core.model.DbRequest;
import com.rocketsoft.core.utils.report.ReportBuilder;
import com.rocketsoft.custom.abs.FormModuleLifecycle;
import com.rocketsoft.custom.annotation.FormModule;
import com.rocketsoft.custom.annotation.OnClient;
import com.rocketsoft.custom.annotation.OnServer;
import com.rocketsoft.custom.ui.Binary;
import com.rocketsoft.module.entity.dictionary.Product;
import com.rocketsoft.module.entity.document.Sell;
import com.rocketsoft.module.entity.quantitativeregistry.Stock;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.List;

@FormModule(entityName = "sell", formName = "Main")
public class MainForm extends FormModuleLifecycle<Sell> {

    /**
     * @doc INIT_CLIENT_METHOD
     * @title initClient
     * @info Is triggered on client side on form open
     */
    @Override
    public void initClient() {
        if (obj.id == null) {
            obj.address = "1000, Kyiv";
        }
    }

    /**
     * @doc INIT_SERVER_METHOD
     * @title initServer
     * @info Is triggered on server side on form open before sending data to client
     */
    @Override
    public void initServer() {
        if (obj.id == null) {
            obj.comment = "Test comment";
            Sell.Products row = new Sell.Products();
            obj.products.add(row);
            row.amount = BigDecimal.TEN;
            row.qty = new BigDecimal("12");
            row.price = new BigDecimal("345.65");
            calculateTotalAmount();
        }
    }

    /**
     * @doc ON_CLIENT_METHOD_ANNOTATION
     * @title @OnClient annotation
     * @info Marks methods that can be invoked on client side.
     * Should not contain any server leogic.
     * If part of logic should be invoked on server side, then need to create separate method with annotation <doc ref="ON_SERVER_METHOD_ANNOTATION"><code>@OnServer</code></doc>
     * <doc ref="EXAMPLE_ONSERVER_FROM_ONCLIENT">Example of using <code>@OnServer</code> from <code>@OnClient</code></doc>
     */
    @OnClient()
    public void productsPriceOrQtyOnChange(Sell.Products row) {
        row.amount = row.qty.multiply(row.price);
        calculateTotalAmount();
    }

    @OnClient
    public void calculateTotalAmount() {
        obj.totalAmount = BigDecimal.ZERO;
        obj.products.forEach(products -> {
            obj.totalAmount = obj.totalAmount.add(products.amount);
        });
    }

    @OnClient()
    public void productsOnRowsChange() {
        calculateTotalAmount();
    }

    @OnClient()
    public void printOnClick() {
        /**
         * @doc EXAMPLE_ONSERVER_FROM_ONCLIENT
         * @title Invoking @OnServer from @OnClient
         * @info
         * Used when some logic <doc ref="ON_CLIENT_METHOD_ANNOTATION">from client</doc> should be invoked <doc ref="ON_SERVER_METHOD_ANNOTATION">on server</doc> side.
         */
        Binary report = printOnServer();
        report.download();
    }

    /**
     * @doc ON_SERVER_METHOD_ANNOTATION
     * @title @OnServer annotation
     * @info Marks methods that can be invoked on server side.
     * Does not have access to form
     * <doc ref="EXAMPLE_ONSERVER_FROM_ONCLIENT">Example of using <code>@OnServer</code> from <code>@OnClient</code></doc>
     */
    @OnServer
    public Binary printOnServer() {
        /**
         * @doc TABLE_REPORT_BUILDER
         * @title Builder of table report
         * @info
         * Used to build table reports based on Excel templates.
         * - Class: <code>ReportBuilder</code>
         * - Constructor parameters:
         * -- template - path to *.xlsx file. This file should be located in folder <i>/src/main/resources/configuration/reportTemplates/</i>. Example <doc path="/src/main/resources/configuration/reportTemplates/tmpReport.xlsx">tmpReport.xlsx</doc>
         * -- headerWidth - number of cells that will be used for header. They are used only to mark report areas and will not be a part of content.
         * -- headerHeight - number of rows that will be used for header. They are used only to mark report areas and will not be a part of content.
         * Methods:
         * - <code>addParam</code> - adds parameter that will be used in template. Parameters:
         * -- name: name of the parameter that should be added
         * -- value: value of the parameter. To get string value method toString will be used
         * - <code>fillParamsFromObject</code> - reads all fields of the provided object and add parameters with their names.
         * - <code>cleanParams</code> - cleans all added parameters. Does not impact already printed areas
         * - <code>buildHorizontalArea</code> - builds horizontal area with using current parameter. Placeholder in selected are should have format <code>${paramName}</code>. Parameters:
         * -- name: name of the area
         */
        ReportBuilder reportBuilder = new ReportBuilder("tmpReport.xlsx", 1, 0);
        reportBuilder.addParam("comment", obj.comment).buildHorizontalArea("HEADER");
        for (Sell.Products row : obj.products) {
            reportBuilder.cleanParams().fillParamsFromObject(row).buildHorizontalArea("TABLE_ROW");
        }
        ByteArrayOutputStream os = reportBuilder.fillParamsFromObject(obj).buildHorizontalArea("FOOTER").build();
        Binary result = Binary.createFromOutputStream(os);
        result.setType(Binary.Type.XLSX);
        result.setName("Report");
        result.setExtension("xlsx");
        return result;
    }

    /**
     * Javadoc
     */
    @OnClient()
    public void productsQtyBeforeCustomSelectFromListButtonOpen(Sell.Products row, List<String> values) {
        values.add("One");
        values.add("Five");
        values.add("Ten");
    }

    @OnClient()
    public void productsQtyAfterCustomSelectFromListButtonChosen(Sell.Products row, String value) {
        switch (value) {
            case "One": {
                row.setQty(new BigDecimal("1"));
                break;
            }
            case "Five": {
                row.setQty(new BigDecimal("5"));
                break;
            }
            case "Ten": {
                row.setQty(new BigDecimal("10"));
                break;
            }
            default:
                row.setQty(BigDecimal.ZERO);
        }
        productsPriceOrQtyOnChange(row);
    }

    public static class CommentItem {

        public final String title;

        public final String value;

        public CommentItem(String title, String value) {
            this.title = title;
            this.value = value;
        }
    }

    @OnClient()
    public void commentBeforeCustomSelectFromListButtonOpen(List<CommentItem> values) {
        values.add(new CommentItem("Short", "It's a short comment"));
        values.add(new CommentItem("Long", "It's a loooooooooooooooooooooooooooong comment"));
    }

    @OnClient()
    public void commentAfterCustomSelectFromListButtonChosen(CommentItem value) {
        obj.setComment(value.value);
    }

    @OnClient()
    public void fillFromStocksOnClick() {
        List<StockData> stocks = getStocks();
        stocks.forEach(stockData -> {
            Sell.Products item = new Sell.Products();
            obj.getProducts().add(item);
            System.out.println(stockData.product.getName());
            item.setProduct(stockData.product);
            item.setQty(stockData.quantity);
        });

    }

    public static class StockData {
        public Product product;
        public BigDecimal quantity;
    }

    @OnServer
    private List<StockData> getStocks() {
        /**
         * @doc DB_REQUEST_LIST_EXAMPLE
         * @info
         * Returns list of custom class <code>StockData</code> instances based on 2 fields from quantitative registry.
         */
        return DbService.requestList(DbRequest.create()
                .withSource(Stock.NAME)
                .withReturnType(StockData.class)
                .withQuantitativeType(DbRequest.QuantitativeType.SUM)
                .withField(Stock.F_product)
                .withField(Stock.F_quantity));
    }
}
