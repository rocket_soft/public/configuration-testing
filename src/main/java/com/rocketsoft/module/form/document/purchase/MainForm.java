package com.rocketsoft.module.form.document.purchase;

import com.rocketsoft.core.app.service.DbService;
import com.rocketsoft.core.model.DbRequest;
import com.rocketsoft.custom.abs.FormModuleLifecycle;
import com.rocketsoft.custom.annotation.FormModule;
import com.rocketsoft.custom.annotation.OnClient;
import com.rocketsoft.custom.annotation.OnServer;
import com.rocketsoft.module.entity.dictionary.Product;
import com.rocketsoft.module.entity.document.Purchase;

import java.util.List;

@FormModule(entityName = "purchase", formName = "Main")
public class MainForm extends FormModuleLifecycle<Purchase> {

    @OnClient()
    public void addLiquidOnClick() {
        List<Product> products = getLiquidProducts();
        products.forEach(product -> {
            Purchase.Items item = new Purchase.Items();
            obj.getItems().add(item);
            item.setProduct(product);
        });
    }

    @OnServer
    public List<Product> getLiquidProducts() {
        /**
         * @doc DB_REQUEST_SIMPLE_LIST
         * @info
         * Returns list of entity <code>Product</code> instances filtered by Product.Type.
         */
        return DbService.requestList(DbRequest.create()
                .withSource(Product.NAME)
                .withFilter(Product.F_type, Product.Type.LIQUID));
    }

    @OnClient()
    public void addBreadOnClick() {
        Product bread = getBreadProduct();

        Purchase.Items item = new Purchase.Items();
        obj.getItems().add(item);
        item.setProduct(bread);
    }

    @OnServer
    public Product getBreadProduct() {
        /**
         * @doc DB_REQUEST_SINGLE
         * @info
         * Returns single <code>Product</code> instances filtered by name.
         */
        return DbService.requestSingle(DbRequest.create()
                .withSource(Product.NAME)
                .withFilter(Product.F_name, "Bread"));
    }
}
