package com.rocketsoft.module.mock;

import com.rocketsoft.core.app.init.InitBean;
import com.rocketsoft.module.entity.dictionary.Product;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Profile("!prod")
public class MockData implements InitBean {

    @Transactional
    public void init() {
        Product milk = new Product();
        milk.setName("Milk");
        milk.setType(Product.Type.LIQUID);
        milk.save();

        Product oil = new Product();
        oil.setName("Oil");
        oil.setType(Product.Type.LIQUID);
        oil.save();

        Product bread = new Product();
        bread.setName("Bread");
        bread.setType(Product.Type.PIECE);
        bread.save();
    }
}
