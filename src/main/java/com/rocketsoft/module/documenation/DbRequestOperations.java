package com.rocketsoft.module.documenation;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DbRequestOperations {

    /**
     * @doc DB_REQUEST_OPERATIONS
     * @title DB Requests
     * @info Used to read data from DB. Returned objects should not be written directly to DB.
     * Class <code>DbService</code>. Methods:
     * - <code>requestList</code> - returns list of records. It can be list of maps or list of specified by <code>DbRequest.withReturnType</code> objects
     * - <code>requestFirst</code> - returns first record. It can be map or specified by <code>DbRequest.withReturnType</code> object
     * - <code>requestSingle</code> - returns value from single columns from the first record. Throws exception if the result has more than 1 column.
     * - <code>requestReport</code> - returns data for report. Is not usually used in code
     * <p>
     * Class <code>DbRequest</code>. Methods:
     * - <code>create</code> (required) - creates new builder of DbRequest instance
     * - <code>withSource</code> (required) - provides source (from which table the data will be retrieved)
     * - <code>withReturnType</code> - provides class of required results. Number and order of field in this class should correspond to requested fields. Names may be different. Fields with type "TABLE" currently are not retrieved from DB. It will be added in future versions.
     * - <code>withTable</code> - provides name of "TABLE" field that also should be retrieved from DB
     * - <code>withField</code> - adds field name that should be retrieved. Has optional parameter <code>Aggregation</code> that allows providing aggregated result. Possible values: SUM, AVG, MAX, MIN
     * - <code>withFilter</code> - adds filter. Parameters:
     * -- <code>path</code> (required) - can have format <i>"dictionary1.dictionary2.name"</i>
     * -- <code>value</code> (required) - value that will be used for filtering
     * -- <code>condition</code> (optional. Default values is "EQUAL") - <code>FilterCondition</code> with possible values: EQUAL, NOT_EQUAL, MORE, MORE_OR_EQUAL, LESS, LESS_OR_EQUAL, IN, CONTAIN, LIKE, ILIKE (case insensitive).
     * - <code>withOrder</code> - sorts results based on specified order. Has parameters:
     * -- <code>path</code> (required) - can have format <i>"dictionary1.dictionary2.name"</i>
     * -- <code>type</code> (required) - <code>OrderType</code> that has values: ASK, DESC.
     * - <code>withQuantitativeType</code> - provides type of grouping quantitative registry records. Used only with quantitative registries. Possible values: SUM, <i>TURNOVER</i> (will be added later)
     * - <code>withHistoricalType</code> - provides type of grouping historical registry records. Used only with historical registries. Possible values: LATEST, EARLIEST
     * - <code>withSourceName</code> - specifies if source name should be added to returned column names. Used only for reports
     * <p>
     * Examples:
     * - <doc ref="DB_REQUEST_SINGLE" action="SHOW_CODE">Request single entity value</doc>
     * - <doc ref="DB_REQUEST_SIMPLE_LIST" action="SHOW_CODE">Minimal request with filters for list of records</doc>
     * - <doc ref="DB_REQUEST_LIST_EXAMPLE" action="SHOW_CODE">Request list of records with specified result type</doc>
     * @doNotShowCode
     */
    private void info() {

    }
}
