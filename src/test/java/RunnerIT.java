import com.rocketsoft.module.App;
import com.rocketsoft.testing.RocketBasicIT;
import org.springframework.context.ConfigurableApplicationContext;

public class RunnerIT extends RocketBasicIT {

    @Override
    public Class getAppClass() {
        return App.class;
    }

    @Override
    public void prepareTestData(ConfigurableApplicationContext context) {

    }
}
